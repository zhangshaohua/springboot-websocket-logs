# 统一日志监控 系统


##  目的:
    
    统一监控 开发测试环境日志   如果需要可以拓展线上环境的日志(自己视情况而定) 或者分环境部署 (区分线下 线上) 
##  步骤
    
    * git clone https://git.oschina.net/redArmy/springboot-websocket-logs.git 
    * 导入 doc下的 ![SQL文件](doc/cloud_monitor_logs_2017-07-26.sql) 修改自己的 application.properties 数据库  
    * mvn spring-boot:run
    * 访问 http://127.0.0.1:8888 (端口自己改) 登录用户目前写在SecurityConfig tomas 123123 可自行拓展     访问url(我是走nginx访问的)
    * 注意 项目部署服务器 需要做到 可以 ssh 免登录到 log 日志所在服务器 (此处可以有别的思路 目前是 ssh 并且 tail 日志文件 可以考虑 消息队列 直接传送到本系统 或者 应用系统 打印日志流 或者文件到指定端口 然后本系统接收 端口流数据 等等 自己可以想想 各种方法 条条大路通罗马)
     
## 系统截图
     
* ![登录](doc/登录.png)
* ![列表](doc/列表.png)
* ![新增](doc/新增.png)
* ![日志](doc/日志.png)

## 附录
    
    nginx config 
    
    
    upstream logs_tomas_cn {
        server 192.168.1.85:8888;
        keepalive 64;
    }
    
    server {
      listen 80;
      server_name logs.xxx.cn;
      index index.html index.jhtml index.htm default.html default.htm;
      root html;
    
      access_log /opt/logs/nginx/logs.tomas.cn.log;
      error_log /opt/logs/nginx/logs.tomas.cn.error.log;
    
      location / {
        proxy_pass http://logs_tomas_cn;
        proxy_redirect off;
        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
      }
    }

## 参考项目 

     https://github.com/wucao/JDeploy
     https://github.com/paurushchandra/springboot-websocket.git 
